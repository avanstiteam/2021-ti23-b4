/* LwIP SNTP example
   This example code is in the Public Domain (or CC0 licensed, at your option.)
   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include "talking_clock.h"
#include <string.h>
#include <time.h>
	
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_event.h"
#include "esp_log.h"
#include "esp_attr.h"

static const char *TAG = "TALKING_CLOCK";


esp_err_t talking_clock_init() {
	
	// Initialize queue
	ESP_LOGI(TAG, "Creating FreeRTOS queue for talking clock");
	talking_clock_queue = xQueueCreate( 10, sizeof( int ) );
	
	if (talking_clock_queue == NULL) {
		ESP_LOGE(TAG, "Error creating queue");
		return ESP_FAIL;
	}
	
	return ESP_OK;
}

esp_err_t talking_clock_fill_queue() {
	
	time_t now;
    struct tm timeinfo;
    time(&now);
	
	char strftime_buf[64];
	localtime_r(&now, &timeinfo);
    strftime(strftime_buf, sizeof(strftime_buf), "%c", &timeinfo);
    ESP_LOGI(TAG, "The current date/time in Amsterdam is: %s", strftime_buf);
	
	
	// Reset queue
	esp_err_t ret = xQueueReset(talking_clock_queue);
	if (ret != ESP_OK) {
		ESP_LOGE(TAG, "Cannot reset queue");
	}
	
	int data = TALKING_CLOCK_ITSNOW_INDEX;
	// Fill queue
	//xQueueSend(talking_clock_queue, &data, portMAX_DELAY);

	int min = timeinfo.tm_min;
	if (min > 0 && min < 15)
	{
		data = TALKING_CLOCK_1_INDEX - 1 + min;
		ret = xQueueSend(talking_clock_queue, &data, portMAX_DELAY);
		data = TALKING_CLOCK_OVER_INDEX;
		ret = xQueueSend(talking_clock_queue, &data, portMAX_DELAY);
	}

	if (min == 15)
	{
		data = TALKING_CLOCK_KWART_INDEX;
		ret = xQueueSend(talking_clock_queue, &data, portMAX_DELAY);
		data = TALKING_CLOCK_OVER_INDEX;
		ret = xQueueSend(talking_clock_queue, &data, portMAX_DELAY);
	}

	if (min > 15 && min < 30)
	{
		data = 30 - min;
		ret = xQueueSend(talking_clock_queue, &data, portMAX_DELAY);
		data = TALKING_CLOCK_VOOR_INDEX;
		ret = xQueueSend(talking_clock_queue, &data, portMAX_DELAY);
		data = TALKING_CLOCK_HALF_INDEX;
		ret = xQueueSend(talking_clock_queue, &data, portMAX_DELAY);
	}

	if (min == 30)
	{
		data = TALKING_CLOCK_HALF_INDEX;
		ret = xQueueSend(talking_clock_queue, &data, portMAX_DELAY);
	}

	if (min > 30 && min < 45)
	{
		data = min - 30;
		ret = xQueueSend(talking_clock_queue, &data, portMAX_DELAY);
		data = TALKING_CLOCK_OVER_INDEX;
		ret = xQueueSend(talking_clock_queue, &data, portMAX_DELAY);
		data = TALKING_CLOCK_HALF_INDEX;
		ret = xQueueSend(talking_clock_queue, &data, portMAX_DELAY);
	}
	
	if (min == 45)
	{
		data = TALKING_CLOCK_KWART_INDEX;
		ret = xQueueSend(talking_clock_queue, &data, portMAX_DELAY);
		data = TALKING_CLOCK_VOOR_INDEX;
		ret = xQueueSend(talking_clock_queue, &data, portMAX_DELAY);
	}

	if (min > 45 && min < 60)
	{
		data = 60 - min;
		ret = xQueueSend(talking_clock_queue, &data, portMAX_DELAY);
		data = TALKING_CLOCK_VOOR_INDEX;
		ret = xQueueSend(talking_clock_queue, &data, portMAX_DELAY);
	
	}

	// Convert hours to AM/PM unit
	int hour = timeinfo.tm_hour;
	hour = (hour == 0 ? 12 : hour);
	hour = (hour > 12 ? hour%12 : hour);
	if (min >= 16)
	{
		if (hour >= 12)
		{
			hour = hour - 12;
			data = TALKING_CLOCK_1_INDEX + hour;
		} else {
			data = TALKING_CLOCK_1_INDEX + hour;
		}
		
		
	} else {
		data = TALKING_CLOCK_1_INDEX-1+hour;
	}
	ret = xQueueSend(talking_clock_queue, &data, portMAX_DELAY);

	if (ret != ESP_OK) {
		//ESP_LOGE(TAG, "Cannot queue data");
		//return ret;
	}

	
	if (min == 0 || min == 60)
	{
		data = TALKING_CLOCK_HOUR_INDEX;
		ret = xQueueSend(talking_clock_queue, &data, portMAX_DELAY);
		if (ret != ESP_OK) {
			//ESP_LOGE(TAG, "Cannot queue data");
			//return ret;
		}
	}
	
	
	ESP_LOGI(TAG, "Queue filled with %d items", uxQueueMessagesWaiting(talking_clock_queue));
	
	return ESP_OK;
}


