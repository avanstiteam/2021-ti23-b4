#ifndef TALKING_CLOCK_H
#define TALKING_CLOCK_H

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "freertos/queue.h"

#ifdef __cplusplus
extern "C" {
#endif

#define TALKING_CLOCK_MAX_STRING 40
#define TALKING_CLOCK_ITEMS 21

#define TALKING_CLOCK_ITSNOW_INDEX 0
#define TALKING_CLOCK_1_INDEX 1
#define TALKING_CLOCK_2_INDEX 2
#define TALKING_CLOCK_3_INDEX 3
#define TALKING_CLOCK_4_INDEX 4
#define TALKING_CLOCK_5_INDEX 5
#define TALKING_CLOCK_6_INDEX 6
#define TALKING_CLOCK_7_INDEX 7
#define TALKING_CLOCK_8_INDEX 8
#define TALKING_CLOCK_9_INDEX 9
#define TALKING_CLOCK_10_INDEX 10
#define TALKING_CLOCK_11_INDEX 11
#define TALKING_CLOCK_12_INDEX 12
#define TALKING_CLOCK_13_INDEX 13
#define TALKING_CLOCK_14_INDEX 14
#define TALKING_CLOCK_HOUR_INDEX 15
#define TALKING_CLOCK_KWART_INDEX 16
#define TALKING_CLOCK_VOOR_INDEX 17
#define TALKING_CLOCK_OVER_INDEX 18
#define TALKING_CLOCK_HALF_INDEX 19
#define TALKING_CLOCK_ALARM_INDEX 20

static char talking_clock_files[TALKING_CLOCK_ITEMS][TALKING_CLOCK_MAX_STRING] = {
	"/sdcard/Het-Is.mp3",
	"/sdcard/1.mp3",
	"/sdcard/2.mp3",
	"/sdcard/3.mp3",
	"/sdcard/4.mp3",
	"/sdcard/5.mp3",
	"/sdcard/6.mp3",
	"/sdcard/7.mp3",
	"/sdcard/8.mp3",
	"/sdcard/9.mp3",
	"/sdcard/10.mp3",
	"/sdcard/11.mp3",
	"/sdcard/12.mp3",
	"/sdcard/13.mp3",
	"/sdcard/14.mp3",
	"/sdcard/uur.mp3",
	"/sdcard/Kwart.mp3",
	"/sdcard/Voor.mp3",
	"/sdcard/Over.mp3",
	"/sdcard/Half.mp3",
	"/sdcard/alarm.mp3"
	
};


QueueHandle_t talking_clock_queue;

esp_err_t talking_clock_init();
esp_err_t talking_clock_fill_queue();

#ifdef __cplusplus
}
#endif

#endif  // TALKING_CLOCK_H
