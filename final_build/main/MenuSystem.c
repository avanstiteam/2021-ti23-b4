#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_log.h"
#include "esp_wifi.h"

#include "nvs_flash.h"
#include "nvs.h"
#include "esp_timer.h"

#include "sdkconfig.h"
#include "audio_element.h"
#include "audio_pipeline.h"
#include "audio_event_iface.h"
#include "audio_common.h"
#include "http_stream.h"
#include "i2s_stream.h"
#include "mp3_decoder.h"
#include "raw_stream.h"
#include "fatfs_stream.h"
#include "filter_resample.h"
#include "input_key_service.h"

#include "esp_peripherals.h"
#include "periph_wifi.h"
#include "board.h"

#include "audio_mem.h"
#include "periph_touch.h"
#include "periph_adc_button.h"
#include "periph_button.h"

#include "MenuSystem.h"
#include "internetRadio.h"
#include "lcd.h"
#include "talking_clock.h"
#include "guitar_tuner.h"
#include "freq_finder.h"
#include "menu.h"
#include "Rotary.h"
#include "snakeLogic.h"
#include "alarmClock.h"

#include "sntp_sync.h"

#include "sdcard_list.h"
#include "sdcard_scan.h"
#include "smbus.h"

static const char *TAG = "MENU";

void loadLCDDisplay(void);
void contextLeft(void);
void contextRight(void);
void okayPressed(void);
void reloadRadioPipeline();
void setStation(audio_element_handle_t);
static audio_element_handle_t create_i2s_stream(int, audio_stream_type_t);
LCDText lcdInfo;
static audio_element_handle_t create_raw_stream();
static audio_element_handle_t create_resample_filter();
void PlayButtonHandler();
void constructRadioDisplayLine(Station Station);
void snakeTimer();
void Stop_Snake_Timer();
static void periodic_timer_callback(void *arg);

int snakeStarted = 0;
esp_timer_handle_t snake_timer;

/* Define the menu items */

static unsigned int currentMenuIndex = 0; /* Start at first menu item */
//static unsigned int currentMenuId;
static unsigned int timeSelection = MODE_MENU;

//Audio
audio_pipeline_handle_t pipelineOutput, pipelineInput;
audio_element_handle_t http_stream_reader, i2s_stream_writer, mp3_decoder, raw_reader, resample_filter, i2s_stream_reader, fatfs_stream_reader;
audio_board_handle_t board_handleTalkingClock;
TimerHandle_t timer_1_sec;
audio_event_iface_handle_t evt;
return_data_t dataPrevious;

//This method creates the timer to play snake on
void snakeTimer() {
	const esp_timer_create_args_t snake_timer_args = {
		.callback = &periodic_timer_callback,
		/* name is optional, but may help identify the timer when debugging */
		.name = "periodic"};

	ESP_ERROR_CHECK(esp_timer_create(&snake_timer_args, &snake_timer));
	/* Start the timers */
	ESP_ERROR_CHECK(esp_timer_start_periodic(snake_timer, 1000000));
}

//This methode is the callback for the snake timer
static void periodic_timer_callback(void *arg) {
	esp_timer_get_time();
	int result = freq_finder_getFreq();
	InputCheck(result);
	updateSnakeOnLCD(getSnake());
	if (getGameState() == 1) {
		Stop_Snake_Timer();
	}

}

//This methode stops the snaketimer when the player is done
void Stop_Snake_Timer() {
	ESP_ERROR_CHECK(esp_timer_stop(snake_timer));
}

//creates the line that displays the radio station on the lcd
void constructRadioDisplayLine(Station station) {
	char NameString[20] = "";
	int namelength = strlen(station.stationNaam);
	int Spaces = 18;
	strcat(NameString, "|");
	strcat(NameString, station.stationNaam);
	if (namelength <= Spaces) {
		Spaces = Spaces - namelength;
		for (int i = 0; i < Spaces; i++) {
			strcat(NameString, " ");
		}
		strcat(NameString, "|");
	}
	strcpy(lcdInfo.row4, NameString);
}


void reloadRadioPipeline(){
	audio_pipeline_stop(pipelineOutput);
    audio_pipeline_wait_for_stop(pipelineOutput);
    audio_pipeline_terminate(pipelineOutput);
    setStation(http_stream_reader);
    audio_pipeline_reset_ringbuffer(pipelineOutput);
    audio_pipeline_reset_elements(pipelineOutput);
	audio_pipeline_run(pipelineOutput);
}

//newer version of the function that sets the radio station
void setStation(audio_element_handle_t reader){
	ESP_LOGI(TAG, "[ * ] Reached This Point in Radio Switch");
	Station newStation = retrieveCurrentStation();
    audio_element_set_uri(reader, newStation.stationURL);
}


//creates the i2s stream
static audio_element_handle_t create_i2s_stream(int sample_rate, audio_stream_type_t type) {
    i2s_stream_cfg_t i2s_cfg = I2S_STREAM_CFG_DEFAULT();
    i2s_cfg.type = type;
    i2s_cfg.i2s_config.sample_rate = sample_rate;
    audio_element_handle_t i2s_stream = i2s_stream_init(&i2s_cfg);
    return i2s_stream;
}

//creates the resampling filter
static audio_element_handle_t create_resample_filter(int source_rate, int source_channels, int dest_rate, int dest_channels) {
    rsp_filter_cfg_t rsp_cfg = DEFAULT_RESAMPLE_FILTER_CONFIG();
    rsp_cfg.src_rate = source_rate;
    rsp_cfg.src_ch = source_channels;
    rsp_cfg.dest_rate = dest_rate;
    rsp_cfg.dest_ch = dest_channels;
    audio_element_handle_t filter = rsp_filter_init(&rsp_cfg);
    return filter;
}

//creates raw stream
static audio_element_handle_t create_raw_stream() {
    raw_stream_cfg_t raw_cfg = {
        .out_rb_size = 8 * 1024,
        .type = AUDIO_STREAM_READER,
    };
    audio_element_handle_t raw_reader = raw_stream_init(&raw_cfg);
    return raw_reader;
}


//alters the pipeline depending on the current state in the statemachine
void checkRegisters(){
	switch(currentMenuIndex){
		case MENU_MAIN_0_ID:
			freeRawBuffer();
			audio_pipeline_stop(pipelineOutput);
			audio_pipeline_wait_for_stop(pipelineOutput);
			audio_pipeline_terminate(pipelineOutput);

			audio_event_iface_discard(evt);

			audio_pipeline_unlink(pipelineOutput);

    		const char *link_tagCase0[3] = {"fatfs", "mp3", "i2s_writer"};
    		audio_pipeline_link(pipelineOutput, &link_tagCase0[0], 3);

			vTaskDelay(10);

			audio_event_iface_cfg_t evt_cfg0 = AUDIO_EVENT_IFACE_DEFAULT_CFG();
			evt = audio_event_iface_init(&evt_cfg0);

			audio_pipeline_set_listener(pipelineOutput, evt);

    		audio_pipeline_reset_ringbuffer(pipelineOutput);
    		audio_pipeline_reset_elements(pipelineOutput);
			audio_pipeline_change_state(pipelineOutput, AEL_STATE_INIT);	
			

			break;

		case MENU_MAIN_1_ID:
			freeRawBuffer();

			audio_pipeline_stop(pipelineOutput);
			audio_pipeline_wait_for_stop(pipelineOutput);
			audio_pipeline_terminate(pipelineOutput);	

			audio_event_iface_discard(evt);

			audio_pipeline_unlink(pipelineOutput);

    		const char *link_tagCase1[3] = {"http", "mp3", "i2s_writer"};
    		audio_pipeline_link(pipelineOutput, &link_tagCase1[0], 3);

			vTaskDelay(10);

			audio_event_iface_cfg_t evt_cfg1 = AUDIO_EVENT_IFACE_DEFAULT_CFG();
			evt = audio_event_iface_init(&evt_cfg1);

			audio_pipeline_set_listener(pipelineOutput, evt);

			audio_pipeline_reset_ringbuffer(pipelineOutput);
    		audio_pipeline_reset_elements(pipelineOutput);
			audio_pipeline_change_state(pipelineOutput, AEL_STATE_INIT);
			audio_pipeline_run(pipelineOutput);
			
			break;	


		case MENU_MAIN_2_ID:

			snake_Freq_config_check();

			audio_pipeline_stop(pipelineOutput);
			audio_pipeline_wait_for_stop(pipelineOutput);
			audio_pipeline_terminate(pipelineOutput);

			vTaskDelay(10);

    		audio_pipeline_run(pipelineInput);

			break;

		case 3:

			Tuner_Freq_config_check();

			audio_pipeline_stop(pipelineOutput);
			audio_pipeline_wait_for_stop(pipelineOutput);
			audio_pipeline_terminate(pipelineOutput);

			vTaskDelay(10);

    		ESP_LOGI(TAG, "Start pipeline");
    		audio_pipeline_run(pipelineInput);

			break;
		
	}
}

//stops the freq_finder
void freq_finder_breakdown() {
    // Clean up (if we somehow leave the while loop, that is...)
    ESP_LOGI(TAG, "Deallocate raw sample buffer memory");
    freeRawBuffer();

    audio_pipeline_stop(pipelineInput);
    audio_pipeline_wait_for_stop(pipelineInput);
    audio_pipeline_terminate(pipelineInput);
    audio_pipeline_unregister(pipelineInput, i2s_stream_reader);
    audio_pipeline_unregister(pipelineInput, resample_filter);
    audio_pipeline_unregister(pipelineInput, raw_reader);

    audio_pipeline_deinit(pipelineInput);
    audio_element_deinit(i2s_stream_reader);
    audio_element_deinit(resample_filter);
    audio_element_deinit(raw_reader);
}
//calls when the rottary is moved
void onRotaryMoved(int16_t movement) {

	if (movement == -1) {
		ESP_LOGI(TAG, "Rotary moved left");
		contextLeft();
	} else if (movement == 1) {
		ESP_LOGI(TAG, "Rotary moved right");
		contextRight();
	}
}

void onRotaryClicked(void) {
	okayPressed();
}

void ModeButtonHandler() {
	contextRight();
}

void RecButtonHandler() {
	contextLeft();
}

void SetButtonHandler(){
	switch(currentMenuIndex){
		case 1:
			increaseStation();
			reloadRadioPipeline();
			loadLCDDisplay();
			break;
	
	}
}

//this code is calles when the play button is pressed on the board
void PlayButtonHandler() {
	audio_element_state_t radio_el_state = audio_element_get_state(i2s_stream_writer);	
	audio_element_state_t clock_el_state = audio_element_get_state(i2s_stream_writer);	

	switch(currentMenuIndex){
		case MENU_MAIN_0_ID:
			switch (clock_el_state) {
				case AEL_STATE_INIT:
					ESP_LOGI(TAG, "[ * ] Starting audio pipeline");
					talking_clock_fill_queue();
					
					audio_element_set_uri(fatfs_stream_reader, talking_clock_files[TALKING_CLOCK_ITSNOW_INDEX]);
					//printf("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\n");
					audio_pipeline_run(pipelineOutput);
					break;	

				case AEL_STATE_RUNNING:
					ESP_LOGI(TAG, "[ * ] Pausing audio pipeline");
					audio_pipeline_pause(pipelineOutput);
					break;

				case AEL_STATE_PAUSED:
					ESP_LOGI(TAG, "[ * ] Resuming audio pipeline");
					talking_clock_fill_queue();
					audio_element_set_uri(fatfs_stream_reader, talking_clock_files[TALKING_CLOCK_ITSNOW_INDEX]); // Set first sample
					audio_pipeline_reset_ringbuffer(pipelineOutput);
					audio_pipeline_reset_elements(pipelineOutput);
					audio_pipeline_change_state(pipelineOutput, AEL_STATE_INIT);
					audio_pipeline_run(pipelineOutput);
					break;
				

				case AEL_STATE_FINISHED:
					ESP_LOGI(TAG, "[ * ] Starting audio pipeline");
					talking_clock_fill_queue();
					audio_element_set_uri(fatfs_stream_reader, talking_clock_files[TALKING_CLOCK_ITSNOW_INDEX]); // Set first sample
					audio_pipeline_reset_ringbuffer(pipelineOutput);
					audio_pipeline_reset_elements(pipelineOutput);
					audio_pipeline_change_state(pipelineOutput, AEL_STATE_INIT);
					audio_pipeline_run(pipelineOutput);
					break;	

				default:
					ESP_LOGI(TAG, "[ * ] Not supported state %d", clock_el_state);
					break;
			}
			
		break;

		case MENU_MAIN_1_ID:
			switch (radio_el_state){
				case AEL_STATE_INIT:
					break;

				case AEL_STATE_STOPPED:
					break;

				case AEL_STATE_PAUSED:
					audio_pipeline_resume(pipelineOutput);
					break;

				case AEL_STATE_RUNNING:
					audio_pipeline_pause(pipelineOutput);
					break;

				default:
					break;
			}

			break;

		case MENU_MAIN_4_ID:
			okayPressed();
		break;
	}
	
}


void stmp_timesync_event(struct timeval *tv) {
    ESP_LOGI(TAG, "Notification of a time synchronization event");

	time_t now;
    struct tm timeinfo;
    time(&now);
	char strftime_buf[64];

	localtime_r(&now, &timeinfo);
    strftime(strftime_buf, sizeof(strftime_buf), "%c", &timeinfo);
    ESP_LOGI(TAG, "The current date/time in Amsterdam is: %s", strftime_buf);
	
}

void timer_1_sec_callback( TimerHandle_t xTimer ){ 
	// Print current time to the screen

	time_t now;
    struct tm timeinfo;
    time(&now);
	
	char strftime_buf[20];
	localtime_r(&now, &timeinfo);
	sprintf(&strftime_buf[0], "%02d:%02d:%02d", timeinfo.tm_hour, timeinfo.tm_min, timeinfo.tm_sec);
	if(currentMenuIndex == MENU_MAIN_0_ID) {
		moveCursor(5,2);
		size_t timeSize = strlen(strftime_buf);
		for (int i = 0; i < timeSize; i++) {
			WriteCharToLCD(strftime_buf[i]);
		}
	}
	
	if(currentMenuIndex == MENU_MAIN_0_ID){
	// Say the time every hour
	if (timeinfo.tm_min == 0 && timeinfo.tm_sec == 0) {
		talking_clock_fill_queue();
		audio_element_set_uri(fatfs_stream_reader, talking_clock_files[TALKING_CLOCK_ITSNOW_INDEX]); // Set first sample
		audio_pipeline_reset_ringbuffer(pipelineOutput);
		audio_pipeline_reset_elements(pipelineOutput);
		audio_pipeline_change_state(pipelineOutput, AEL_STATE_INIT);
		audio_pipeline_run(pipelineOutput);
	}
	//Say time every half hour
    if (timeinfo.tm_min == 30 && timeinfo.tm_sec == 0) {
        talking_clock_fill_queue();
		audio_element_set_uri(fatfs_stream_reader, talking_clock_files[TALKING_CLOCK_ITSNOW_INDEX]); // Set first sample
		audio_pipeline_reset_ringbuffer(pipelineOutput);
		audio_pipeline_reset_elements(pipelineOutput);
		audio_pipeline_change_state(pipelineOutput, AEL_STATE_INIT);
		audio_pipeline_run(pipelineOutput);
    }

	} else if (currentMenuIndex == MENU_MAIN_4_ID) {
		if (timeinfo.tm_hour == currentAlarm.alarmHour && timeinfo.tm_min == currentAlarm.alarmMinute && timeinfo.tm_sec == 0 && timeSelection == MODE_MENU) {
		talking_clock_fill_queue();
		audio_element_set_uri(fatfs_stream_reader, talking_clock_files[TALKING_CLOCK_ALARM_INDEX]); // Set alarm sample
		audio_pipeline_reset_ringbuffer(pipelineOutput);
		audio_pipeline_reset_elements(pipelineOutput);
		audio_pipeline_change_state(pipelineOutput, AEL_STATE_INIT);
		audio_pipeline_run(pipelineOutput);
		}
	}
	
}
//checks all presable buttons buttons
static esp_err_t input_key_service_cb(periph_service_handle_t handle, periph_service_event_t *evt, void *ctx){
	/* Handle touch pad events
           to start, pause, resume, finish current song and adjust volume
        */
	audio_board_handle_t board_handle = (audio_board_handle_t)ctx;
	int player_volume;
	audio_hal_get_volume(board_handle->audio_hal, &player_volume);

	if (evt->type == INPUT_KEY_SERVICE_ACTION_CLICK_RELEASE) {
		ESP_LOGI(TAG, "[ * ] input key id is %d", (int)evt->data);
		switch ((int)evt->data) {
			case INPUT_KEY_USER_ID_PLAY:
				ESP_LOGI(TAG, "[ * ] [Play] input key event");
				PlayButtonHandler();
				break;

			case INPUT_KEY_USER_ID_SET:
				SetButtonHandler();
        	    break;

			case INPUT_KEY_USER_ID_MODE:
				ModeButtonHandler();
				checkRegisters();
				break;

			case INPUT_KEY_USER_ID_REC:
				RecButtonHandler();
				checkRegisters();
				break;

			default:
				break;	
		}
	}

	return ESP_OK;
}

int menu_main() {

	esp_err_t err = nvs_flash_init();
	if (err == ESP_ERR_NVS_NO_FREE_PAGES) {
		ESP_ERROR_CHECK(nvs_flash_erase());
		err = nvs_flash_init();
	}

	esp_netif_init();

	init_guitar_mode();

	// Setup logging level
	esp_log_level_set("*", ESP_LOG_INFO);
	esp_log_level_set(TAG, ESP_LOG_INFO);

	esp_periph_config_t periph_cfgTalkingClock = DEFAULT_ESP_PERIPH_SET_CONFIG();
	esp_periph_set_handle_t set= esp_periph_set_init(&periph_cfgTalkingClock);
	audio_board_key_init(set);
	audio_board_sdcard_init(set, SD_MODE_1_LINE);
	board_handleTalkingClock = audio_board_init();
	audio_hal_ctrl_codec(board_handleTalkingClock->audio_hal, AUDIO_HAL_CODEC_MODE_BOTH, AUDIO_HAL_CTRL_START);
	rawBufferInit();

    periph_wifi_cfg_t wifi_cfg = {
        .ssid = CONFIG_WIFI_SSID,
        .password = CONFIG_WIFI_PASSWORD,
    };

    
    esp_periph_handle_t wifi_handle = periph_wifi_init(&wifi_cfg);
    esp_periph_start(set, wifi_handle);
    periph_wifi_wait_for_connected(wifi_handle, portMAX_DELAY);

	input_key_service_info_t input_key_info[] = INPUT_KEY_DEFAULT_INFO();
	input_key_service_cfg_t input_cfg = INPUT_KEY_SERVICE_DEFAULT_CONFIG();
	input_cfg.handle = set;
	periph_service_handle_t input_ser = input_key_service_create(&input_cfg);
	input_key_service_add_key(input_ser, input_key_info, INPUT_KEY_NUM);
	periph_service_set_callback(input_ser, input_key_service_cb, (void *)board_handleTalkingClock);
	audio_pipeline_cfg_t pipeline_cfg = DEFAULT_AUDIO_PIPELINE_CONFIG();
	pipelineOutput = audio_pipeline_init(&pipeline_cfg);
	mem_assert(pipelineOutput);
	pipelineInput = audio_pipeline_init(&pipeline_cfg);
	mem_assert(pipelineInput);
    http_stream_cfg_t http_cfg = HTTP_STREAM_CFG_DEFAULT();
    http_stream_reader = http_stream_init(&http_cfg);
    i2s_stream_cfg_t i2s_cfg = I2S_STREAM_CFG_DEFAULT();
    i2s_cfg.type = AUDIO_STREAM_WRITER;
    i2s_stream_writer = i2s_stream_init(&i2s_cfg);
    mp3_decoder_cfg_t mp3_cfg = DEFAULT_MP3_DECODER_CONFIG();
    mp3_decoder = mp3_decoder_init(&mp3_cfg);
    char *urlTalkingClock = NULL;
    fatfs_stream_cfg_t fatfs_cfg = FATFS_STREAM_CFG_DEFAULT();
    fatfs_cfg.type = AUDIO_STREAM_READER;
    fatfs_stream_reader = fatfs_stream_init(&fatfs_cfg);
    audio_element_set_uri(fatfs_stream_reader, urlTalkingClock);

	i2s_stream_reader = create_i2s_stream(AUDIO_SAMPLE_RATE, AUDIO_STREAM_READER);
    resample_filter = create_resample_filter(AUDIO_SAMPLE_RATE, 2, GOERTZEL_SAMPLE_RATE_HZ, 1);
	raw_reader = create_raw_stream();

	audio_pipeline_register(pipelineOutput, http_stream_reader, "http");
    audio_pipeline_register(pipelineOutput, mp3_decoder,        "mp3");
    audio_pipeline_register(pipelineOutput, i2s_stream_writer,  "i2s_writer");
	audio_pipeline_register(pipelineOutput, fatfs_stream_reader,  "fatfs");

    audio_pipeline_register(pipelineInput, i2s_stream_reader, "i2s_reader");
    audio_pipeline_register(pipelineInput, resample_filter, "rsp_filter");
    audio_pipeline_register(pipelineInput, raw_reader, "raw");

    const char *link_tagOutput[3] = {"fatfs", "mp3", "i2s_writer"};
    audio_pipeline_link(pipelineOutput, &link_tagOutput[0], 3);

    const char *link_tagInput[3] = {"i2s_reader", "rsp_filter", "raw"};
    audio_pipeline_link(pipelineInput, &link_tagInput[0], 3);

	setStation(http_stream_reader);

	audio_event_iface_cfg_t evt_cfg = AUDIO_EVENT_IFACE_DEFAULT_CFG();
	evt = audio_event_iface_init(&evt_cfg);
	audio_pipeline_set_listener(pipelineOutput, evt);
	talking_clock_init();
	
	// Synchronize NTP time		
	sntp_sync(stmp_timesync_event);
	// Setup first audio sample 'it's now'
	audio_element_set_uri(fatfs_stream_reader, talking_clock_files[TALKING_CLOCK_ITSNOW_INDEX]);
	
	init_lcd();
	loadLCDDisplay();

int id = 1;
//ROTARY
	TimerHandle_t timer_100_ms = xTimerCreate("MyTimer0.1s", pdMS_TO_TICKS(100), pdTRUE, (void *)id, &timer_100_ms_callback);
     if (xTimerStart(timer_100_ms, 10) != pdPASS) {
         ESP_LOGE(TAG, "Cannot start 0.1 second timer");
     }

	timer_1_sec = xTimerCreate("MyTimer", pdMS_TO_TICKS(1000), pdTRUE, ( void * )id, &timer_1_sec_callback);
	if( xTimerStart(timer_1_sec, 10 ) != pdPASS ) {
		ESP_LOGE(TAG, "Cannot start 1 second timer");
    }

    RotaryEncoder_init();
    RotaryEncoder_setColorRed(0);
    RotaryEncoder_setColorGrn(100);
    RotaryEncoder_setColorBlu(255);

	while (1) {
		audio_event_iface_msg_t msg;
		//unused VAR
		audio_event_iface_listen(evt, &msg, 10);
		if (currentMenuIndex == 0) {
			//printf("dankjewel martijn\n");
				if (msg.source_type == AUDIO_ELEMENT_TYPE_ELEMENT) {
       			// Set music info for a new song to be played
       			if (msg.source == (void *) mp3_decoder
       			    && msg.cmd == AEL_MSG_CMD_REPORT_MUSIC_INFO) {
       			    audio_element_info_t music_info = {0};

					audio_element_getinfo(mp3_decoder, &music_info);
					//printf("%d\n", err);
       			    ESP_LOGI(TAG, "[ * ] Received music info from mp3 decoder, sample_rates=%d, bits=%d, ch=%d",
       			             music_info.sample_rates, music_info.bits, music_info.channels);
					audio_element_setinfo(i2s_stream_writer, &music_info);
       			    continue;	
       			}
       			//audio_element_state_t el_state = audio_element_get_state(i2s_stream_writerTalkingClock);
       			audio_element_state_t el_state = audio_element_get_state(i2s_stream_writer);
				if (el_state == AEL_STATE_FINISHED) {
					int element = 0;
					if (uxQueueMessagesWaiting(talking_clock_queue) > 0 && 
						xQueueReceive(talking_clock_queue, &element, portMAX_DELAY)) {
						ESP_LOGI(TAG, "Finish sample, towards next sample");
						urlTalkingClock = talking_clock_files[element];
						ESP_LOGI(TAG, "URL: %s", urlTalkingClock);
						audio_element_set_uri(fatfs_stream_reader, urlTalkingClock);
						audio_pipeline_reset_ringbuffer(pipelineOutput);
						audio_pipeline_reset_elements(pipelineOutput);
						audio_pipeline_change_state(pipelineOutput, AEL_STATE_INIT);
						audio_pipeline_run(pipelineOutput);
					} else {
						// No more samples. Pause for now
						audio_pipeline_pause(pipelineOutput);
					}
				}
       			    continue;
       		}
		}

		if (currentMenuIndex == 1) {
			//printf("pepeJam\n");
			if (msg.source_type == AUDIO_ELEMENT_TYPE_ELEMENT && msg.source == (void *)mp3_decoder && msg.cmd == AEL_MSG_CMD_REPORT_MUSIC_INFO) {
				audio_element_info_t music_info = {0};
				audio_element_getinfo(mp3_decoder, &music_info);

				ESP_LOGI(TAG, "[ * ] Receive music info from mp3 decoder, sample_rates=%d, bits=%d, ch=%d",
						 music_info.sample_rates, music_info.bits, music_info.channels);

				audio_element_setinfo(i2s_stream_writer, &music_info);
				i2s_stream_set_clk(i2s_stream_writer, music_info.sample_rates, music_info.bits, music_info.channels);
				continue;
			}
		}

		if (currentMenuIndex == 2) {
			freq_finder_loop(raw_reader);
			int result = freq_finder_getFreq();
		
			if (result == 440 && snakeStarted == 0) {
				//start snake game + init
				//printf("START SNAKE\n");
				initSnake();
				snakeTimer();

				snakeStarted = 1;
			}
			int GameResult = getGameState();
			if (GameResult == 1) {
				Stop_Snake_Timer();
				snakeStarted = 0;
			}
			
		}

		if (currentMenuIndex == 3) {
			freq_finder_loop(raw_reader);

			return_data_t data = loop_tune();
			
			if (!compareDataStructGitar(data, dataPrevious)){
				printMenuItemOnLcd(currentMenuIndex, lcdInfo);
				for (short i = 0; i < 20; i++) {
					lcdInfo.row2[i] = data.graphic[i];
					lcdInfo.row3[i] = data.tuneName[i];
				}
			}
			dataPrevious = data;
		}

		vTaskDelay(10);
	}
	return 1;
}

//calls when the ok button is pressed
void okayPressed(void) {
	switch (currentMenuIndex) {
	case MENU_MAIN_0_ID:
		PlayButtonHandler();
		break;
	case MENU_MAIN_1_ID:
		PlayButtonHandler();
		ESP_LOGI("Menu", "Radio okay menu pressed !\n");
		break;
	case MENU_MAIN_4_ID:
		if (timeSelection == MODE_MENU) {
			timeSelection = MODE_HOUR;
		} else if (timeSelection == MODE_HOUR) {
			timeSelection = MODE_MINUTE;
		} else if (timeSelection == MODE_MINUTE) {
			timeSelection = MODE_MENU;
		}
		writeAlarmOnLCD(currentAlarm.alarmHour, currentAlarm.alarmMinute, timeSelection);
		break;

	}
}
//when the left button is pressed, either with the rotary or the button 
void contextLeft(void) {
	if(timeSelection == MODE_MENU) {
		if (currentMenuIndex == 0) {
			currentMenuIndex = 4;
		} else {
			currentMenuIndex--;
		}
		clearLCD();
		loadLCDDisplay();
	} else {
		selectNewTime(timeSelection, DIRECTION_LEFT);
	}
}
//when the Right button is pressed, either with the rotary or the button 
void contextRight(void) {
	if(timeSelection == MODE_MENU){
		currentMenuIndex++;
		if (currentMenuIndex > 4){
			currentMenuIndex = 0;
		}
		clearLCD();
		loadLCDDisplay();
	} else {
		selectNewTime(timeSelection, DIRECTION_RIGHT);
	}
}
//start the lcd
void loadLCDDisplay() {
	MENU_ITEM_STRUCT newMenuDisplay = retrieveMenu(currentMenuIndex);
	strcpy(lcdInfo.row1, newMenuDisplay.text[0]);
	strcpy(lcdInfo.row2, newMenuDisplay.text[1]);
	strcpy(lcdInfo.row3, newMenuDisplay.text[2]);
	strcpy(lcdInfo.row4, newMenuDisplay.text[3]);

	if (currentMenuIndex == 1)
	{
		constructRadioDisplayLine(retrieveCurrentStation());
	}
	printMenuItemOnLcd(currentMenuIndex, lcdInfo);

}