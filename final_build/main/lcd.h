#ifndef LCD_H
#define LCD_H

#include "snakeLogic.h"

#define LCD_MAX_LINES 4

#define LCD_NUM_ROWS 4
#define LCD_NUM_COLUMNS 40
#define LCD_NUM_VIS_COLUMNS 20
#define LCD_ADDRESS 0x27

typedef struct
{
	char row1[100]; /* IDs to jump to on keypress */
	char row2[100];
	char row3[100];
	char row4[100];
} LCDText;

void init_lcd(void);
void clearLCD();
void printMenuItemOnLcd(int, LCDText);
void updateSnakeOnLCD(Snake);
void writeAlarmOnLCD(int, int, int);
void WriteCharToLCD(char);
void moveCursor(int x, int y);

#endif