#ifndef GUITAR_TUNER_H
#define GUITAR_TUNER_H

#define GRAPHIC_SIZE 21

typedef struct guitar_tuner_return_data{
    char tuneName[21];
    char graphic[GRAPHIC_SIZE];
}return_data_t;

typedef struct guitar_tune
{
  char tuneName[21];
  int target_freq;

}tune_t;

tune_t notes[13];


return_data_t loop_tune();
int compareDataStructGitar(return_data_t struct1, return_data_t struct2);
#endif