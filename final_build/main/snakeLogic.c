#define RAND_MAX 80

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "snakeLogic.h"
#include <unistd.h>
#include "esp_timer.h"
#include "esp_log.h"
#include "esp_sleep.h"
#include "sdkconfig.h"
#include <time.h>
#include <string.h>

static const char *TAG = "SNAKE LOGIC";

Snake snake;
int globalSoundValue;
esp_timer_handle_t periodic_timer;

int GameState;

void generateAppel(void);
void generateStartPosition(void);
void increaseSnakeSize(void);
void checkSnakeGameStates();
void UpdateSnakePos(LCDPOS newHead);


//this method starts the game with base values.
void initSnake(void)
{
    generateStartPosition();
    srand(time(0)); // Initialization, should only be called once.
    generateAppel();
    snake.snakeSize = MinSnakeSize;
    snake.direction = right;
    GameState = GameRunning;
}

//This methode retrieves the state of the game
int getGameState()
{
    return GameState;
}

//This methode checks which input is given, if invalid it will continue in its original direction
void InputCheck(int inputDir)
{
    switch (inputDir)
    {
    case up:
        //printf("Direction UP\n");
        SnakeUp();
        break;
    case down:
        //printf("Direction DOWN\n");
        SnakeDown();
        break;
    case left:
        //printf("Direction LEFT\n");
        SnakeLeft();
        break;
    case right:
        //printf("Direction RIGHT\n");
        SnakeRight();
        break;
    default:
        InputCheck(snake.direction);
        break;
    }
}

//This methode generates an appel on a non snake location
void generateAppel(void)
{
    LCDPOS newApple;

    int r = rand(); // Returns a pseudo-random integer between 0 and RAND_MAX
    //printf("Random snake value : %d",r);
    newApple.xPos = (r % 20) - 1;
    newApple.yPos = ((r % 80)/ 20);

    if (newApple.xPos == -1) 
    {
        newApple.xPos = LCDMaxBorder_wide;
    }
    for (int i = 0; i < snake.snakeSize; i++)
    {
        if (newApple.xPos == snake.snakeParts[i].xPos && newApple.yPos == snake.snakeParts[i].yPos)
        {
            generateAppel();
            return;
        }
    }

    snake.apple = newApple;
}

//this methode generates the starting position of the snake for the beginning of the game.
void generateStartPosition(void)
{
    snake.snakeHead.xPos = 5;
    snake.snakeHead.yPos = 2;
    LCDPOS snake1;
    snake1.xPos = 5;
    snake1.yPos = 2;
    LCDPOS snake2;
    snake2.xPos = 4;
    snake2.yPos = 2;
    snake.snakeParts[0] = snake1;
    snake.snakeParts[1] = snake2;
}

//This methode increases the snakesize, sends the snkae to update and generates a new apple
void increaseSnakeSize(void)
{
    snake.snakeSize++;

    UpdateSnakePos(snake.apple);
    generateAppel();
}

//This methode updates the snakes position to an empty spot or onto the apple when eaten.
void UpdateSnakePos(LCDPOS newHead)
{
    LCDPOS Mover;
    for (int i = snake.snakeSize; i > 1; i--)
    {
        Mover = snake.snakeParts[i - 2];
        snake.snakeParts[i - 1] = Mover;
    }
    snake.snakeParts[0] = newHead;
}

//This methode moves the snake up unless its already moving down
void SnakeUp(void)
{
    if (snake.direction != down)
    {
        snake.direction = up;
        if (snake.snakeHead.yPos == LCDMinBorder_long)
        {
            snake.snakeHead.yPos = LCDMaxBorder_long;
        }
        else
        {
            snake.snakeHead.yPos--;
        }
        checkSnakeGameStates();
    }else 
    {
        SnakeDown();
    }   
}

//This methode moves the snake down unless its already moving up
void SnakeDown(void)
{
    if (snake.direction != up)
    {
        snake.direction = down;
        if (snake.snakeHead.yPos == LCDMaxBorder_long)
        {
            snake.snakeHead.yPos = LCDMinBorder_long;
        }
        else
        {
            snake.snakeHead.yPos++;
        }
        checkSnakeGameStates();
    }else 
    {
        SnakeUp();
    }
    
}

//This methode moves the snake left unless its already moving right
void SnakeLeft(void)
{
    if (snake.direction != right)
    {
        snake.direction = left;
        if (snake.snakeHead.xPos == LCDMinBorder_wide)
        {
            snake.snakeHead.xPos = LCDMaxBorder_wide;
        }
        else
        {
            snake.snakeHead.xPos--;
        }
        checkSnakeGameStates();
    }else 
    {
        SnakeRight();
    }
}

//This method moves the snake right unless its already moving left
void SnakeRight(void)
{
    if (snake.direction != left)
    {
        snake.direction = right;
        if (snake.snakeHead.xPos == LCDMaxBorder_wide)
        {
            snake.snakeHead.xPos = LCDMinBorder_wide;
        }
        else
        {
            snake.snakeHead.xPos++;
        }
        checkSnakeGameStates();
    }else 
    {
        SnakeLeft();
    }
}

//This method retrieves the snake
Snake getSnake()
{
    return snake;
}

//This method checks whether the game needs to change state. 
//If the snake bites itself it loses.
//If its 80 long it wins.
void checkSnakeGameStates()
{
    //printf("snake pos %d : %d, apple pos %d : %d\n", snake.snakeHead.xPos, snake.snakeHead.yPos, snake.apple.xPos, snake.apple.yPos);

    if (snake.apple.xPos == snake.snakeHead.xPos && snake.apple.yPos == snake.snakeHead.yPos)
    {
        printf("APPLE EAT\n");
        increaseSnakeSize();
        if (snake.snakeSize == MaxSnakeSize)
        {
            //TODO: game win?
            GameState = GameFinished;
        }
    }
    else
    {
        UpdateSnakePos(snake.snakeHead);
    }
    for (int i = 1; i < snake.snakeSize; i++)
    {
        if (snake.snakeHead.xPos == snake.snakeParts[i].xPos && snake.snakeHead.yPos == snake.snakeParts[i].yPos)
        {
            //TODO: game end
            GameState = GameFinished;
        }
    }
}
