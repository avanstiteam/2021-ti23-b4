/* Play an MP3 file from HTTP

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#include <string.h>
#include "internetRadio.h"

static unsigned int currentStationIndex = 0;
static const char *TAG = "InternetRadio";
// audio_element_handle_t http_stream_reader, i2s_stream_writer, mp3_decoder;
Station arrayStations[MAX_Stations] = {
	{ 
		"radio1",
			"https://icecast.omroep.nl/radio1-bb-mp3"
		},
		{
			"3fm",
			"https://icecast.omroep.nl/3fm-bb-mp3"
		},
		{
			"radio2",
			"https://icecast.omroep.nl/radio2-bb-mp3"
		},
		{
			"radio4",
			"https://icecast.omroep.nl/radio4-bb-mp3"
		},
		{
			"funx-dance",
			"https://icecast.omroep.nl/funx-dance-bb-mp3"
		}
};


Station retrieveCurrentStation(){
    return arrayStations[currentStationIndex];
}
//go to next station
void increaseStation(){
	currentStationIndex++;
	if (currentStationIndex >= MAX_Stations)
	{
		currentStationIndex = 0;
	}
}

