#include <string.h>
#include <stdio.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_log.h"
#include "esp_wifi.h"

#include "esp_peripherals.h"
#include "periph_wifi.h"
#include "board.h"

#include "audio_mem.h"
#include "periph_touch.h"
#include "periph_adc_button.h"
#include "periph_button.h"
#include "freq_finder.h"


#include "MenuSystem.h"
#include "freertos/timers.h"

int id = 15;
static const char *TAG = "MAIN";

int app_main(void){

    menu_main();

    return 1;
}