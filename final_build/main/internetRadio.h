#ifndef Internet_Radio_H
#define Internet_Radio_H

#define MAX_Stations 5

typedef struct {
	char stationNaam[20];			/* ID for this item */
	char stationURL[50];	/* IDs to jump to on keypress */
} Station;

Station retrieveCurrentStation();
void increaseStation();
#endif