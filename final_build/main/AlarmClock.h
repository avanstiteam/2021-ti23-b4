#ifndef ALARMCLOCK_H
#define ALARMCLOCK_H


#define MODE_MENU 0
#define MODE_HOUR 1
#define MODE_MINUTE 2

#define DIRECTION_LEFT 0
#define DIRECTION_RIGHT 1

typedef struct 
{
    int alarmHour;
    int alarmMinute;
}CurrentAlarmTime_t;

CurrentAlarmTime_t currentAlarm;

void alarmClock_init();

void selectNewTime(int mode, int direction);

#endif