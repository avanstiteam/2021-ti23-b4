#ifndef MENUStruct_H
#define MENUStruct_H


/* Define indices for the keys in the array of new IDs */
#define MENU_KEY_OK 0
#define MENU_KEY_LEFT 1
#define MENU_KEY_RIGHT 2
#define CONTEXT_KEY_LEFT 3
#define CONTEXT_KEY_RIGHT 4
#define CONTEXT_KEY_UP 5
#define CONTEXT_KEY_DOWN 6


/* Define the IDs for all menu items */
#define MENU_MAIN_0_ID 0
#define MENU_MAIN_1_ID 1
#define MENU_MAIN_2_ID 2
#define MENU_MAIN_3_ID 3
#define MENU_MAIN_4_ID 4
#define MENU_SUB_1_0_ID 20
#define MENU_SUB_1_4_ID 24

#define MAX_MENU_KEY 11

typedef struct {
	unsigned int id;			/* ID for this item */
	unsigned int newId[MAX_MENU_KEY];	/* IDs to jump to on keypress */
	char* text[4];		/* Text for this item */
} MENU_ITEM_STRUCT;

MENU_ITEM_STRUCT retrieveMenu(int);



#endif


