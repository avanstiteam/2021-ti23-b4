#include <string.h>
#include <stdio.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_log.h"
#include "esp_wifi.h"

#include "esp_peripherals.h"
#include "periph_wifi.h"
#include "board.h"

#include "AlarmClock.h"
#include "i2c-lcd1602.h"
#include "MenuSystem.h"
#include "lcd.h"


void alarmClock_init(){
    currentAlarm.alarmHour = 0;
    currentAlarm.alarmMinute = 0;
    writeAlarmOnLCD(currentAlarm.alarmHour, currentAlarm.alarmMinute, MODE_MENU);
}

void selectNewTime(int mode, int direction){
    if(mode == MODE_HOUR)
    {
        if (direction == DIRECTION_LEFT) 
        {
            if (currentAlarm.alarmHour <= 0)    //Makes sure time can't go below 0
            {
                currentAlarm.alarmHour = 23;
            }else
            {
                currentAlarm.alarmHour--;
            }
        }else if (direction == DIRECTION_RIGHT)
        {
            if (currentAlarm.alarmHour >= 23)   //Makes sure time can't go above 23
            {
                currentAlarm.alarmHour = 0;
            }else
            {
                currentAlarm.alarmHour ++;
            }
            
        }
    }else if (mode == MODE_MINUTE)
    {
        if (direction == DIRECTION_LEFT)
        {
            if (currentAlarm.alarmMinute <= 0)  //Makes sure time can't go below 0
            {
                currentAlarm.alarmMinute = 59;
            }else
            {
                currentAlarm.alarmMinute--;
            }
        }else if (direction == DIRECTION_RIGHT)
        {
            if (currentAlarm.alarmMinute >= 59)     //Makes sure time can't go above 59
            {
                currentAlarm.alarmMinute = 0;
            }else
            {
                currentAlarm.alarmMinute ++;
            }
        }
    }
    writeAlarmOnLCD(currentAlarm.alarmHour, currentAlarm.alarmMinute, mode);
}