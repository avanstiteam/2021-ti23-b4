#include "menu.h"


MENU_ITEM_STRUCT menu[] = {
	{ /* Menu item 0 */
		MENU_MAIN_0_ID,
		{ /* New item id (Ok, Left, Right, ContextLeft, ContextRight, ContextUp, ContextDown) */
			MENU_MAIN_0_ID,
			MENU_MAIN_4_ID,
			MENU_MAIN_1_ID,
			MENU_MAIN_0_ID,
			MENU_MAIN_0_ID,
			MENU_MAIN_0_ID,
			MENU_MAIN_0_ID
		},
		{
			/* Menu item text */
			"|    Main Menu     |",
			"| Tijd:            |",
			"|                  |",
			"|                  |"
		}
	},
	{ /* Menu item 1: InternetRadio */
		MENU_MAIN_1_ID,
		{ /* New item id (Ok, Left, Right, ContextLeft, ContextRight, ContextUp, ContextDown) */
			MENU_MAIN_1_ID,
			MENU_MAIN_0_ID,
			MENU_MAIN_2_ID,
			MENU_MAIN_1_ID,
			MENU_MAIN_1_ID,
			MENU_MAIN_1_ID,
			MENU_MAIN_1_ID
		},
		{
			/* Menu item text
			 01234567890123456789 */
			"|      Radio       |",
			"| Station:         |",
			"| Mhz: %d MHZ      |",
			"|                  |"
		}
	},
	{ /* Menu item 2: Snake */
		MENU_MAIN_2_ID,
		{ /* New item id (Ok, Left, Right, ContextLeft, ContextRight, ContextUp, ContextDown) */
			MENU_MAIN_2_ID,
			MENU_MAIN_1_ID,
			MENU_MAIN_3_ID,
			MENU_MAIN_2_ID,
			MENU_MAIN_2_ID,
			MENU_MAIN_2_ID,
			MENU_MAIN_2_ID
		},
		{
			/* Menu item text
			 01234567890123456789 */
			"|                  |",
			"|      Snake       |",
			"|  440HZ to play   |",
			"|                  |"
		}
	},
	{ /* Menu item 3: Gitaar Stemmer */
		MENU_MAIN_3_ID,
		{ /* New item id (Ok, Left, Right, ContextLeft, ContextRight, ContextUp, ContextDown) */
			MENU_MAIN_3_ID,
			MENU_MAIN_2_ID,
			MENU_MAIN_4_ID,
			MENU_MAIN_3_ID,
			MENU_MAIN_3_ID,
			MENU_MAIN_3_ID,
			MENU_MAIN_3_ID
		},
		{
			/* Menu item text
			 01234567890123456789 */
			"|  Gitaar stemmer  |",
			"| Doelfrequentie:  |",
			"|  %DF             |",
			"|<--------+------->|"

		}
	},
	{ /* Menu item 4: Wekker */
		MENU_MAIN_4_ID,
		{ /* New item id (Ok, Left, Right, ContextLeft, ContextRight, ContextUp, ContextDown) */
			MENU_MAIN_4_ID,
			MENU_MAIN_3_ID,
			MENU_MAIN_0_ID,
			MENU_MAIN_4_ID,
			MENU_MAIN_4_ID,
			MENU_MAIN_4_ID,
			MENU_MAIN_4_ID
		},
		{
			/* Menu item text
			 01234567890123456789 */
			"|      Wekker      |",
			"|  Alarm:          |",
			"|                  |",
			"|                  |"

		}
	}
};



MENU_ITEM_STRUCT retrieveMenu(int position){
    return menu[position];
}