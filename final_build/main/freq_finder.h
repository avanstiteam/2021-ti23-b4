#ifndef FREQ_FINDER_H
#define FREQ_FINDER_H

#include "audio_element.h"

#define GOERTZEL_SAMPLE_RATE_HZ 8000 // Sample rate in [Hz]
#define GOERTZEL_FRAME_LENGTH_MS 100 // Block length in [ms]

#define GOERTZEL_BUFFER_LENGTH (GOERTZEL_FRAME_LENGTH_MS * GOERTZEL_SAMPLE_RATE_HZ / 1000) // Buffer length in samples

#define GOERTZEL_DETECTION_THRESHOLD 50.0f // Detect a tone when log magnitude is above this value

#define AUDIO_SAMPLE_RATE 48000 // Audio capture sample rate [Hz]

#define GOERTZEL_NR_FREQS (550 / 2)

typedef enum Mode {guitar_mode,voice_mode, raw_mode, snake_mode }tuning_mode_enum_t;


int freq_finder_getSnakeFreq();

int freq_finder_getFreq();

void init_guitar_mode();

void freq_finder_loop(audio_element_handle_t);

void snake_Freq_config_check();

void Tuner_Freq_config_check();

void freeRawBuffer();

void rawBufferInit();

#endif