#ifndef SNAKELOGIC_H
#define SNAKELOGIC_H

typedef struct
{
    int xPos;
    int yPos;
}LCDPOS;


typedef struct
{
    LCDPOS apple;
    LCDPOS snakeParts[80];
    LCDPOS snakeHead;
    int snakeSize;
    int direction;
}Snake;

#define LCDMaxBorder_wide 19
#define LCDMinBorder_wide 0
#define LCDMaxBorder_long 3
#define LCDMinBorder_long 0

#define MaxSnakeSize 80
#define MinSnakeSize 2

#define GameRunning 0
#define GameFinished 1

#define up 200
#define right 250
#define down 300
#define left 350


//This methode allows for the snake to be started with base values
void initSnake(void);

//This methode retrieves the state of the game,
int getGameState(void);

//This methode checks the input of the player
void InputCheck(int);

//This methode moves the snake up
void SnakeUp(void);

//This methode moves the snake down
void SnakeDown(void);

//This methode retrieves the snake
Snake getSnake(void);

//This methode moves the snake left
void SnakeLeft(void);

//This methode moves the snake right
void SnakeRight(void);

//This methode moves the snake up
void checkSnakeGameStates(void);

#endif