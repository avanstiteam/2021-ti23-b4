#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "freq_finder.h"
#include "guitar_tuner.h"
#include "goertzel_filter.h"
#include "esp_log.h"
#include "audio_element.h"
#include "audio_pipeline.h"
#include "audio_event_iface.h"
#include "raw_stream.h"
static const char *TAG = "Freq";

extern tune_t notes[13];
int last_freq;


int GOERTZEL_DETECT_FREQS[550 / 2];

int16_t *raw_buffer;

goertzel_filter_cfg_t filters_cfg[GOERTZEL_NR_FREQS];
goertzel_filter_data_t filters_data[GOERTZEL_NR_FREQS];

typedef struct tuning_mode
{
    int count;
    int filters[550 / 2];
} tune_mode_t;

int GOERTZEL_DETECT_FREQS[] = {
    200,
    250,
    300,
    350,
    440};

int fr_numbers = 0;
float highestf = 0;
int highest;

int guitar_notes[240];


int freq_finder_getSnakeFreq() {
    int temp = last_freq;
    last_freq = 0;
    return temp;
}

int freq_finder_getFreq() {
    return last_freq;
}

//creates test frequencies for guitar
void init_guitar_mode() {
    int notePointer = 0;
    int length = sizeof(notes) / sizeof(tune_t);

    for (short i = 0; i < length; i++) {
        for (short j = notes[i].target_freq - 10; j < notes[i].target_freq + 10; j++) {
            guitar_notes[notePointer] = j;
            notePointer++;
            fr_numbers++;
        }
    }
}

void detect_freq(int target_freq, float magnitude) {
    float logMagnitude = 10.0f * log10f(magnitude);
    if (logMagnitude > GOERTZEL_DETECTION_THRESHOLD) {
        if (logMagnitude > highestf) {
            highestf = logMagnitude;
            if (target_freq != 0) {
                ESP_LOGI(TAG, "Detection at frequency %d Hz (magnitude %.2f, log magnitude %.2f)", target_freq, magnitude, logMagnitude);
                last_freq = target_freq;
            }
        }
    }
}

//the loop for finding a new top frequency
void freq_finder_loop(audio_element_handle_t raw_reader) {
	printf("loool\n");
    highest = 0;
    highestf = 0;
    raw_stream_read(raw_reader, (char *)raw_buffer, GOERTZEL_BUFFER_LENGTH * sizeof(int16_t));
    for (int f = 0; f < GOERTZEL_NR_FREQS; f++) {
        float magnitude;

        esp_err_t error = goertzel_filter_process(&filters_data[f], raw_buffer, GOERTZEL_BUFFER_LENGTH);
        ESP_ERROR_CHECK(error);

        if (goertzel_filter_new_magnitude(&filters_data[f], &magnitude)) {
            detect_freq(filters_cfg[f].target_freq, magnitude);
        }
    }
}

void snake_Freq_config_check(){
			for (int f = 0; f < 5; f++) {	
        	    filters_cfg[f].sample_rate = GOERTZEL_SAMPLE_RATE_HZ;
        	    filters_cfg[f].target_freq = GOERTZEL_DETECT_FREQS[f];
        	    filters_cfg[f].buffer_length = GOERTZEL_BUFFER_LENGTH;
        	    esp_err_t error = goertzel_filter_setup(&filters_data[f], &filters_cfg[f]);
        	    ESP_ERROR_CHECK(error);
        	}	
}

void Tuner_Freq_config_check(){
			for (int f = 0; f < fr_numbers; f++) {
    		    filters_cfg[f].sample_rate = GOERTZEL_SAMPLE_RATE_HZ;
    		    filters_cfg[f].target_freq = guitar_notes[f];
    		    filters_cfg[f].buffer_length = GOERTZEL_BUFFER_LENGTH;
    		    esp_err_t error = goertzel_filter_setup(&filters_data[f], &filters_cfg[f]);
    		    ESP_ERROR_CHECK(error);
    		}
}

void freeRawBuffer(){
    free(raw_buffer);
}

void rawBufferInit(){
    raw_buffer = (int16_t *)malloc((GOERTZEL_BUFFER_LENGTH * sizeof(int16_t)));
    if (raw_buffer == NULL) {
       printf("error creating buffer");
    }
   
}