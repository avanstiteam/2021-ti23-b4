#include "freq_finder.h"
#include "guitar_tuner.h"
#include "string.h"
#include "math.h"
#include "stdlib.h"
#include "stdio.h"




//notes to which the guitar needs to be tuned
tune_t notes[] = {  {"D2                  \0",73},
                    {"E2                  \0",83},   //Eddie 
                    {"A2                  \0",110},  //Ate
                    {"D3                  \0",147},  //Dynamite
                    {"G3                  \0",196},  //Good
                    {"B3                  \0",247},  //Bye
                    {"                  DO\0",261},
                    {"                  RE\0",293},
                    {"E4                MI\0",329},   //Eddie
                    {"                  FA\0",349},
                    {"                 SOL\0",391},
                    {"                  LA\0",440},
                    {"                  SI\0",493}
                    };
tune_t closest_note;



//returns the difference in the 
int note_difference(tune_t tune,int freq){
    return abs(tune.target_freq-freq);
}

//loops to find the most prominent tune in this loop
return_data_t loop_tune(){

    int freq = freq_finder_getFreq();

    int smallest_note_difference = note_difference(notes[0],freq);

    closest_note = notes[0];

    for (int i = 1; i < 13; i++)
    {
        int difference = note_difference(notes[i],freq);
        if (difference < smallest_note_difference)
        {
          smallest_note_difference = difference;
          closest_note = notes[i];
        }
    }

//copy string to returndata
    return_data_t returnData;
    for (size_t i = 0; i < 20; i++)
    {
        returnData.tuneName[i] = closest_note.tuneName[i];
    }

    int steps = freq-closest_note.target_freq;

    steps /= 3;

    returnData.graphic[0] = '|';  
    returnData.graphic[19] = '|';     
    returnData.graphic[9] = 'v';
   
    if (steps == 0)
    {
        char *graphicP = "|--------V--------|";
        for (size_t i = 0; i < 20; i++)
        {
            returnData.graphic[i] = graphicP[i];
        }
    }

    //if the frequency is lower than the target frequency
    if (steps < 0)
    {
        if (steps<-8)
        {
            steps = -8;
        }
        
          steps = abs(steps);   

        for (size_t i = 1; i < 9-steps; i++)
        {
           returnData.graphic[i]= '-';
        }
        for (size_t i = 9-steps; i < 9; i++)
        {
            returnData.graphic[i]= '<';
        }
       

        for (size_t i = 10; i < 19; i++)
        {
           returnData.graphic[i] = '-';
        }
       
    //if frequency is higher than the target frequency
    }else if (steps > 0)
    {
         if (steps>8)
        {
            steps = 8;
        }

        for (size_t i = 1; i < 9; i++)
        {
              returnData.graphic[i] = '-';
        }
        for (size_t i = 10; i < 10+steps; i++)
        {
             returnData.graphic[i]= '>';
        }
        for (size_t i = 10+steps; i < 19; i++)
        {
            returnData.graphic[i] = '-';
        }
    }
    return returnData;
}

//for efficiency sakes when the display does nit need to be updated
int compareDataStructGitar(return_data_t struct1, return_data_t struct2){
	for (int i = 0; i < 21; i++)
	{
		if (struct1.graphic[i] != struct2.graphic[i])
		{
			return 0;
		} 
	}
	return 1;
}
