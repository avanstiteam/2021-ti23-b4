#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "i2c-lcd1602.h"
#include "smbus.h"
#include "lcd.h"
#include <time.h>
#include "snakeLogic.h"
#include "menu.h"
#include "AlarmClock.h"

#define LCD_MAX_LINES 4

#define LCD_NUM_ROWS 4
#define LCD_NUM_COLUMNS 40
#define LCD_NUM_VIS_COLUMNS 20
#define LCD_ADDRESS 0x27

// LCD
smbus_info_t *smbus_info = NULL;
i2c_lcd1602_info_t *lcd_info = NULL;

void init_lcd(void)
{
	    // Set up the SMBus
    smbus_info = smbus_malloc();
    smbus_init(smbus_info, I2C_NUM_0, LCD_ADDRESS);
    smbus_set_timeout(smbus_info, 1000 / portTICK_RATE_MS);

    // Set up the LCD1602 device with backlight off
    lcd_info = i2c_lcd1602_malloc();
    i2c_lcd1602_init(lcd_info, smbus_info, true, LCD_NUM_ROWS, LCD_NUM_COLUMNS, LCD_NUM_VIS_COLUMNS);

    // Write first line
    clearLCD();

    uint8_t body[8] = {0x15, 0xa, 0x15, 0xa, 0x15, 0xa, 0x15};
    i2c_lcd1602_define_char(lcd_info, I2C_LCD1602_CHARACTER_CUSTOM_1, body);

    uint8_t Appel[8] = {0x6, 0x4, 0xe, 0x1f, 0x1f, 0xe, 0x0};
    i2c_lcd1602_define_char(lcd_info, I2C_LCD1602_CHARACTER_CUSTOM_2, Appel);

    uint8_t HeadLeft[8] = {0x1f, 0x15, 0x7, 0x1f, 0x7, 0x17, 0x1f};
    i2c_lcd1602_define_char(lcd_info, I2C_LCD1602_CHARACTER_CUSTOM_3, HeadLeft);

    uint8_t HeadRight[8] = {0x1f, 0x15, 0x1c, 0x1f, 0x1c, 0x1d, 0x1f};
    i2c_lcd1602_define_char(lcd_info, I2C_LCD1602_CHARACTER_CUSTOM_4, HeadRight);

    uint8_t HeadUp[8] = {0x4, 0x15, 0x15, 0x1f, 0x17, 0x1f, 0x1f};
    i2c_lcd1602_define_char(lcd_info, I2C_LCD1602_CHARACTER_CUSTOM_5, HeadUp);

    uint8_t HeadDown[8] = {0x1f, 0x1f, 0x1d, 0x1f, 0x15, 0x15, 0x4};
    i2c_lcd1602_define_char(lcd_info, I2C_LCD1602_CHARACTER_CUSTOM_6, HeadDown);
}

void updateSnakeOnLCD(Snake snake)
{
    //First the LCD is cleared
    clearLCD();

    //Next all snake pieces are written
    for (int i = 0; i < snake.snakeSize; i++)
    {
        i2c_lcd1602_move_cursor(lcd_info, snake.snakeParts[i].xPos, snake.snakeParts[i].yPos);
        i2c_lcd1602_write_char(lcd_info, I2C_LCD1602_CHARACTER_CUSTOM_1);
    }

    //Next the appel is drawn
    i2c_lcd1602_move_cursor(lcd_info, snake.apple.xPos, snake.apple.yPos);
    i2c_lcd1602_write_char(lcd_info, I2C_LCD1602_INDEX_CUSTOM_2);

    //Finally the snakehead is drawn in
    i2c_lcd1602_move_cursor(lcd_info, snake.snakeHead.xPos, snake.snakeHead.yPos);
    switch (snake.direction)
    {
    case up:
        i2c_lcd1602_write_char(lcd_info, I2C_LCD1602_INDEX_CUSTOM_5);
        break;
    case down:
        i2c_lcd1602_write_char(lcd_info, I2C_LCD1602_INDEX_CUSTOM_6);
        break;
    case left:
        i2c_lcd1602_write_char(lcd_info, I2C_LCD1602_INDEX_CUSTOM_3);
        break;
    case right:
        i2c_lcd1602_write_char(lcd_info, I2C_LCD1602_INDEX_CUSTOM_4);
        break;
    default:
        i2c_lcd1602_write_char(lcd_info, I2C_LCD1602_INDEX_CUSTOM_4);
        break;
    }
}

void printMenuItemOnLcd(int current, LCDText menuInfo)
{
	clearLCD();

	//printf("%s\n", lcdInfo.row1);
	i2c_lcd1602_move_cursor(lcd_info, 0, 0);
	i2c_lcd1602_write_string(lcd_info, menuInfo.row1);

	i2c_lcd1602_move_cursor(lcd_info, 0, 1);
	i2c_lcd1602_write_string(lcd_info, menuInfo.row2);
	
	if (current == MENU_MAIN_4_ID)
	{
		writeAlarmOnLCD(currentAlarm.alarmHour, currentAlarm.alarmMinute, MODE_MENU);
	}

	i2c_lcd1602_move_cursor(lcd_info, 0, 2);
	i2c_lcd1602_write_string(lcd_info, menuInfo.row3);

	i2c_lcd1602_move_cursor(lcd_info, 0, 3);
	i2c_lcd1602_write_string(lcd_info, menuInfo.row4);

	vTaskDelay(10);
}

void clearLCD(){
	    i2c_lcd1602_clear(lcd_info);
}


void writeAlarmOnLCD(int hour, int minute, int timeSelection){
	char strftime_buf[20];

	sprintf(&strftime_buf[0], "%02d:%02d", hour, minute);
	
	i2c_lcd1602_set_cursor(lcd_info, false);	//Putting these two lines here is a bit ugly, however it prevents weird cursor jumping in the LCD
	i2c_lcd1602_set_blink(lcd_info, false);

	i2c_lcd1602_move_cursor(lcd_info, 9, 1);
	size_t timeSize = strlen(strftime_buf);
	for (int i = 0; i < timeSize; i++)
	{
			i2c_lcd1602_write_char(lcd_info, strftime_buf[i]);
	}
	//Creates the blinking cursor under the selected item
	if (timeSelection == MODE_HOUR)
	{
		i2c_lcd1602_set_cursor(lcd_info, true);
		i2c_lcd1602_set_blink(lcd_info, true);
		i2c_lcd1602_move_cursor(lcd_info, 10, 1);
	}else if (timeSelection == MODE_MINUTE)
	{
		i2c_lcd1602_set_cursor(lcd_info, true);
		i2c_lcd1602_set_blink(lcd_info, true);
		i2c_lcd1602_move_cursor(lcd_info, 13, 1);
	}else{
		i2c_lcd1602_set_cursor(lcd_info, false);
		i2c_lcd1602_set_blink(lcd_info, false);
	}
}

void WriteCharToLCD(char item){
	i2c_lcd1602_write_char(lcd_info, item);
}

void moveCursor(int x, int y){
	i2c_lcd1602_move_cursor( lcd_info, x, y);
}